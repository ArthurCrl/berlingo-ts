import { afterEach, beforeEach, describe, expect, test, vi } from 'vitest';
import { Logger, LogStartEnd } from './logDecorator';

// tslint:disable:max-classes-per-file
// tslint:disable: no-console
let nbHrTimeCalls: number = 0
describe('Testing the start-end  log decorator', () => {

  const logger: Logger = {
    debug: (arg: any) => console.debug(arg),
    error: (arg: any) => console.error(arg),
    info: (arg: any) => console.info(arg),
    perf: (arg: any) => console.debug(arg),
    warn: (arg: any) => console.warn(arg),
  }

  beforeEach(() => {

    vi.spyOn(process, 'hrtime').mockImplementation(() => {
      return [0, (++nbHrTimeCalls) * 1_000_000]
    })
  })

  afterEach(() => {

    vi.clearAllMocks()
    nbHrTimeCalls = 0
  })

  test('it should be able log a synchronous fonction', () => {
    class Test {
      @LogStartEnd(logger, {
        level: 'info',
        withArgs: true,
        withResult: true,
        withTime: true,
        withPrefix: true,
      })
      public exec(arg1: any, arg2: any) {
        console.info('Executing my fonction')
        return 'result'
      }
    }

    const test = new Test()
    const spy = vi.spyOn(console, 'info')
    test.exec('intput1', 'input2')
    expect(spy.mock.calls).toMatchSnapshot()

  })

  test('it should be able log a asynchronous fonction', async () => {

    class Test {
      @LogStartEnd(logger, {
        level: 'info',
        withArgs: true,
        withResult: true,
        withTime: true,

      })
      public async exec(arg1: any, arg2: any) {
        console.info('Executing my asynchronous fonction')
        return 'result'
      }
    }

    const test = new Test()
    const spy = vi.spyOn(console, 'info')
    await test.exec('intput1', 'input2')
    expect(spy.mock.calls).toMatchSnapshot()

  })
})
