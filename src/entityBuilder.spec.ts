import { describe, expect, test } from 'vitest';

import EntityBuilder from './entityBuilder';
import Ingredient, { EKind } from './useCases/business/entities/Ingredient';
import Recipe, { EDifficulty } from './useCases/business/entities/Recipe';

describe('Testing the entity builder', () => {
  describe('Builder creation', () => {
    test('it should be able to create a new builder', () => {
      const result = new EntityBuilder(Ingredient)
      expect(result).toBeDefined()
      expect((result as any).creator).toEqual(Ingredient)
      expect((result as any).builtEntity).toBeInstanceOf(Ingredient)
    })

    test('it should be able to create a new builder from static method', () => {
      const result = EntityBuilder.getA(Ingredient)
      expect(result).toBeDefined()
      expect((result as any).creator).toEqual(Ingredient)
      expect((result as any).builtEntity).toBeInstanceOf(Ingredient)

      const result2 = EntityBuilder.getAn(Ingredient)
      expect(result2).toBeDefined()
      expect((result2 as any).creator).toEqual(Ingredient)
      expect((result2 as any).builtEntity).toBeInstanceOf(Ingredient)
    })
  })

  describe('Entity creation', () => {
    test('it should be able to create a new entity', () => {
      const builtEntity = EntityBuilder.getA(Ingredient)
        .withKind(EKind.Condiment)
        .withName('ketchup')
        .withWeightInGrams(3)
        .build()
      expect(builtEntity).toBeDefined()
      expect(builtEntity).toBeInstanceOf(Ingredient)
      expect(builtEntity.name).toEqual('ketchup')
      expect(builtEntity.kind).toEqual(EKind.Condiment)
      expect(builtEntity.weightInGrams).toEqual(3)
      expect(builtEntity.volumeInMilliliters).not.toBeDefined()
    })

    test('it should be able to create two new entity successively', () => {
      const builder = EntityBuilder.getA(Ingredient)
      const firstBuiltEntity = builder
        .withKind(EKind.Condiment)
        .withWeightInGrams(3)
        .withName('ketchup')
        .build()
      expect(firstBuiltEntity).toBeDefined()
      expect(firstBuiltEntity).toBeInstanceOf(Ingredient)
      expect(firstBuiltEntity.name).toEqual('ketchup')
      expect(firstBuiltEntity.kind).toEqual(EKind.Condiment)
      expect(firstBuiltEntity.weightInGrams).toEqual(3)
      expect(firstBuiltEntity.volumeInMilliliters).not.toBeDefined()

      const secondBuiltEntity = builder
        .withKind(EKind.Fruit)
        .withName('apple')
        .withVolumeInMilliliters(2)
        .build()
      expect(secondBuiltEntity).toBeDefined()
      expect(secondBuiltEntity).toBeInstanceOf(Ingredient)
      expect(secondBuiltEntity.kind).toEqual(EKind.Fruit)
      expect(secondBuiltEntity.weightInGrams).not.toBeDefined()
      expect(secondBuiltEntity.volumeInMilliliters).toEqual(2)

    })

    test('it should be able to copy from correctly', () => {
      const builder = EntityBuilder.getA(Ingredient)
      const firstBuiltEntity = builder
        .withKind(EKind.Condiment)
        .withName('ketchup')
        .withWeightInGrams(3)
        .build()
      expect(firstBuiltEntity).toBeDefined()
      expect(firstBuiltEntity).toBeInstanceOf(Ingredient)
      expect(firstBuiltEntity.name).toEqual('ketchup')
      expect(firstBuiltEntity.kind).toEqual(EKind.Condiment)
      expect(firstBuiltEntity.weightInGrams).toEqual(3)
      expect(firstBuiltEntity.volumeInMilliliters).not.toBeDefined()

      const secondBuiltEntity = builder.byCopyFrom(firstBuiltEntity).build()
      expect(secondBuiltEntity).toBeDefined()
      expect(secondBuiltEntity).toBeInstanceOf(Ingredient)
      expect(secondBuiltEntity.kind).toEqual(EKind.Condiment)
      expect(secondBuiltEntity.weightInGrams).toEqual(3)
      expect(secondBuiltEntity.volumeInMilliliters).not.toBeDefined()

      firstBuiltEntity.kind = EKind.Liquid
      expect(secondBuiltEntity.kind).toEqual(EKind.Condiment)

    })

    test('it should be able to copy deeply from correctly', () => {

      const subEntity = EntityBuilder.getA(Ingredient)
        .withKind(EKind.Condiment)
        .withWeightInGrams(3)
        .withName('An ingredient')
        .build()
      expect(subEntity).toBeDefined()
      expect(subEntity).toBeInstanceOf(Ingredient)
      expect(subEntity.kind).toEqual(EKind.Condiment)
      expect(subEntity.weightInGrams).toEqual(3)
      expect(subEntity.volumeInMilliliters).not.toBeDefined()

      const builder = EntityBuilder.getA(Recipe)
      const firstBuiltEntity = builder
        .withIngredients([subEntity])
        .withDifficulty(EDifficulty.Easy)
        .withSteps([])
        .withName('welsh')
        .withPreparationDurationInMinutes(0)
        .build()
      expect(firstBuiltEntity).toBeDefined()
      expect(firstBuiltEntity).toBeInstanceOf(Recipe)
      expect(firstBuiltEntity.ingredients).toEqual([subEntity])

      const secondBuiltEntity = builder.byCopyFrom(firstBuiltEntity).build()
      expect(secondBuiltEntity).toBeDefined()
      expect(secondBuiltEntity).toBeInstanceOf(Recipe)
      expect(secondBuiltEntity.ingredients).toEqual([subEntity])
      firstBuiltEntity.ingredients[0].kind = EKind.Fruit
      expect(secondBuiltEntity.ingredients[0].kind).toEqual(EKind.Condiment)

    })

    describe('it should be able to build a new entity from a structured input', () => {
      test('it should return an entity when passing no inputs', () => {
        const result  = EntityBuilder.getA(Ingredient).fromInputs().build();
        expect(result).toBeDefined();
      })  

      test('it should return an entity when passing no inputs', () => {
        const result = EntityBuilder.getA(Ingredient)
        .fromInputs({
          kind: EKind.Cheese,
          weightInGrams: 1,
        }).build();
        expect(result).toBeDefined();
        expect(result.kind).toEqual(EKind.Cheese);
        expect(result.numberOfPiece).toBeUndefined();
        expect(result.weightInGrams).toEqual(1);
      })
    })
  })
})
