
export enum EKind {
  Liquid = 0,
  Vegetable = 1,
  Fruit = 2,
  Meat = 3,
  Fish = 4,
  Cheese = 5,

  Condiment = 6,
  Other = 7

}
export default class Ingredient {
  public kind: EKind
  public name: string
  public weightInGrams?: number
  public volumeInMilliliters?: number

  public volumeInSpoon?: number

  public numberOfPiece?: number

}
