import EntityBuilder from 'src/entityBuilder'
import Ingredient, { EKind } from 'src/useCases/business/entities/Ingredient'
import Recipe, { EDifficulty } from 'src/useCases/business/entities/Recipe'
const cheddar = EntityBuilder.getAn(Ingredient)
  .withKind(EKind.Cheese)
  .withWeightInGrams(170)
  .withName('Cheddar')
  .build()

const mustard = EntityBuilder.getAn(Ingredient)
  .withKind(EKind.Condiment)
  .withVolumeInSpoon(1)
  .withName('Mustard')
  .build()

const bier = EntityBuilder.getAn(Ingredient)
  .withKind(EKind.Liquid)
  .withVolumeInMilliliters(20)
  .withName('Belgian bier')
  .build()

const bread = EntityBuilder.getAn(Ingredient)
  .withKind(EKind.Other)
  .withNumberOfPiece(1)
  .withName('Rustic bread')
  .build()

const ham = EntityBuilder.getAn(Ingredient)
  .withKind(EKind.Meat)
  .withNumberOfPiece(1)
  .withName('White ham')
  .build()

const whelshRecipe: Recipe = EntityBuilder.getA(Recipe)
  .withDifficulty(EDifficulty.Easy)
  .withName('Welsh')
  .withPreparationDurationInMinutes(15)
  .withCookingDurationInMinutes(10)
  .withIngredients([cheddar, mustard, bier, bread, ham])
  .withSteps([
    'Faire fondre le cheddar avec la bière, la moutarde, le sel, le poivre dans une casserole, jusqu\'à obtenir une pâte.',
    'Tremper la tartine de pain de campagne dans la bière',
    'Placer la tranche de jambon sur la tranche de pain dans un petit ramequin ou dans une assiette creuse.',
    'Verser le cheddar sur la tranche de pain.',
    'Faire dorer le plat au four au grill , puis servir chaud avec une bonne bière ;-)',
    'Option pour les gourmands : placer un oeuf au plat sur le welch.'
  ])
  .build()
